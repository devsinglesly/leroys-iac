deploy: migration-run app backend nginx db grafana loki

app:
	docker stack deploy -c ./leroys-app/leroys-app.yaml leroys-app
backend:
	docker stack deploy -c ./leroys-backend/leroys-backend.yaml leroys-backend
nginx:
	docker stack deploy -c ./leroys-nginx/nginx.yaml leroys-nginx
db:
	docker stack deploy -c ./leroys-db/db.yaml leroys-postgres
grafana:
	docker stack deploy -c ./Grafana/grafana.yaml leroys-grafana
loki:
	docker stack deploy -c ./Loki/loki.yaml leroys-loki

migration-run:
	docker pull registry.ledius.space/leroys.io/leroys-backend:main
	docker service rm leroys-backend-migration
	docker service create \
	--name=leroys-backend-migration \
	--network leroys \
	--restart-condition=none \
	--env-file=./leroys-backend/.env \
	registry.ledius.space/leroys.io/leroys-backend:main \
	npm run typeorm migration:run -- -d ./dist/data-source.js

build-nginx:
	docker build -t registry.ledius.space/leroys/nginx:main -f ./leroys-nginx/Dockerfile leroys-nginx
push-nginx:
	docker push registry.ledius.space/leroys/nginx:main
